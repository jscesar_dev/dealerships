import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map.component';
import { MapRoutingModule } from './map-routing.module';
import { MatButtonModule } from '@angular/material/button';



@NgModule({
  declarations: [
    MapComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MapRoutingModule
  ]
})
export class MapModule { }
