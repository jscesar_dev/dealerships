import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Map, View } from 'ol'
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import {fromLonLat} from 'ol/proj';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Point from 'ol/geom/Point';
import Feature from 'ol/Feature';
import Overlay from 'ol/Overlay';

import { Subject, takeUntil } from 'rxjs';
import { Dealership } from '../model/dealership.model';
import { GetDealerships } from '../store/actions/dealership.action';
import { AppState } from '../store/app.state';
import { Router } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {

  selectedDealership: Dealership;
  map: Map;
  notifier = new Subject();
  popup: Overlay;

  constructor(private store: Store<AppState>, private router: Router) { }

  ngOnInit(): void {

    this.map = new Map({
      view: new View({
        center: [0, 0],
        zoom: 1,
      }),
      layers: [
        new TileLayer({
          preload: Infinity,
          source: new OSM(),
        }),
      ],
      target: 'map'
    });

  }

  ngAfterViewInit() {

    this.map.on('pointermove',  (e) => {
      let hasFeature = false;
      
      this.map.forEachFeatureAtPixel(e.pixel, (feature) => {
        this.selectedDealership = feature.get('data');
        this.popup.setPosition(e.coordinate);
        hasFeature = true;
      });

      
      if (!hasFeature && this.popup) {
        this.popup.setPosition(null);
      }

    });

    this.popup = new Overlay({
      element: document.getElementById('popup'),
      className: 'popup-overlay'
    });


    this.map.addOverlay(this.popup);
    this.getDealerships();


  }

  getDealerships(): void {
    this.store.dispatch(new GetDealerships())
    this.store.select(store => store.dealerships.dealerships)
      .pipe(
        takeUntil(this.notifier)
      )
      .subscribe(res => {
        let data = []
        if (res.length > 0) {

          data = JSON.parse(JSON.stringify(res)) as Dealership[];

          data.forEach(ele => {

            if (ele.cars?.length > 0) {
              ele.amountCars = ele.cars.length;
             
            } else {
              ele.amountCars = 0
            }

          })
          
        }
        
        if(data.length > 0) {
          this.createMarkers(data);
        }
        
      })


  }

  createMarkers(dealerships: Dealership[]): void {
    const features = []

    for (const iterator of dealerships) {
      const ft = new Feature({
        geometry: new Point(fromLonLat([iterator.location.lon, iterator.location.lat])),
        data: iterator
      });

      features.push(ft);

    }

    const layer = new VectorLayer({
      source: new VectorSource({
          features
      })
    });
    
    if(this.map) {
      
      this.map.addLayer(layer);
     

    }

  }


  gotoDealership():void {
    this.router.navigate(['/dealerships',this.selectedDealership.id])
  }



}
