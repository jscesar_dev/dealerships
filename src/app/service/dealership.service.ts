import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Dealership } from '../model/dealership.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class DealershipService {
	
	constructor(private http: HttpClient) { }

	get(id: number): Observable<Dealership> {
		return this.http.get<Dealership>(`${environment.api}/dealerships/${id}`);
	}

	find(): Observable<Dealership[]> {
		let params = new HttpParams();
		params = params.append('_embed', 'cars')

		return this.http.get<Dealership[]>(`${environment.api}/dealerships`, {params});
	}

	put(id: number, dealership: Dealership): Observable<Dealership> {
		return this.http.put<Dealership>(`${environment.api}/dealerships/${id}`, dealership);
	}

	post(dealership: Dealership): Observable<Dealership> {
		return this.http.post<Dealership>(`${environment.api}/dealerships`, dealership);
	}

	delete(id: number): Observable<number> {
		return this.http.delete<number>(`${environment.api}/dealerships/${id}`);
	}
    
}