import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Car } from '../model/car.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class CarService {
	
	constructor(private http: HttpClient) { }

	get(id: string): Observable<Car> {
		return this.http.get<Car>(`${environment.api}/cars/${id}`);
	}

	find(id:string): Observable<Car[]> {
		return this.http.get<Car[]>(`${environment.api}/dealerships/${id}/cars`);
	}

	put(id: string, car: Car): Observable<Car> {
		return this.http.put<Car>(`${environment.api}/cars/${id}`, car);
	}

	post(dealershipId: string, car: Car): Observable<Car> {
		return this.http.post<Car>(`${environment.api}/cars`, car);
	}

	delete(id: string): Observable<string> {
		return this.http.delete<string>(`${environment.api}/cars/${id}`);
	}
    
}