import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { dealershipsReducer } from './reducers/dealership.reducer';
import { DealershipEffects } from './effects/dealership.effects';
import { carsReducer } from './reducers/car.reducer';
import { CarEffects } from './effects/car.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot({
      dealerships: dealershipsReducer,
      cars: carsReducer
    },
    {
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      },
    }),
    EffectsModule.forRoot([
      DealershipEffects,
      CarEffects
    ]),
    StoreDevtoolsModule.instrument({ maxAge: 50 })
  ]
})
export class StateModule { }
