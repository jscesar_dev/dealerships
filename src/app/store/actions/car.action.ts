import { Action } from '@ngrx/store';
import { Car } from 'src/app/model/car.model';


export enum CarsActionTypes {
  GET_CARS = 'Get Cars',
  GET_CARS_SUCCESS = 'Get Cars Success',
  GET_CARS_ERROR = 'Get Cars Fail',

  POST_CARS = 'Post Cars',
  POST_CARS_SUCCESS = 'Post Cars Success',
  POST_CARS_ERROR = 'Post Cars Fail',

  UPDATE_CARS = 'Update Cars',
  UPDATE_CARS_SUCCESS = 'Update Cars Success',
  UPDATE_CARS_ERROR = 'Update Cars Fail',

  DELETE_CARS = 'Delete Cars',
  DELETE_CARS_SUCCESS = 'Delete Cars Success',
  DELETE_CARS_ERROR = 'Delete Cars Fail'
}

export class GetCars implements Action {
  readonly type = CarsActionTypes.GET_CARS;
  constructor(public id:string) {}
}
export class GetCarsSuccess implements Action {
  readonly type = CarsActionTypes.GET_CARS_SUCCESS;
  constructor( public payload: Car[]) {}
}
export class GetCarsError implements Action {
  readonly type = CarsActionTypes.GET_CARS_ERROR;
  constructor(public payload: Error) {}
}

export class PostCars implements Action {
  readonly type = CarsActionTypes.POST_CARS;
  constructor(public dealershipId:string, public payload: Car) {}
}
export class PostCarsSuccess implements Action {
  readonly type = CarsActionTypes.POST_CARS_SUCCESS;
  constructor(public payload: Car) {}
}
export class PostCarsError implements Action {
  readonly type = CarsActionTypes.POST_CARS_ERROR;
  constructor(public payload: Error) {}
}

export class UpdateCars implements Action {
  readonly type = CarsActionTypes.UPDATE_CARS;
  constructor(public id:string ,public payload: Car) {}
}
export class UpdateCarsSuccess implements Action {
  readonly type = CarsActionTypes.UPDATE_CARS_SUCCESS;
  constructor(public payload: Car) {}
}
export class UpdateCarsError implements Action {
  readonly type = CarsActionTypes.UPDATE_CARS_ERROR;
  constructor(public payload: Error) {}
}

export class DeleteCars implements Action {
  readonly type = CarsActionTypes.DELETE_CARS;
  constructor(public id: string) {}
}
export class DeleteCarsSuccess implements Action {
  readonly type = CarsActionTypes.DELETE_CARS_SUCCESS;
  constructor(public id: string) {}
}
export class DeleteCarsError implements Action {
  readonly type = CarsActionTypes.DELETE_CARS_ERROR;
  constructor(public payload: Error) {}
}

export type CarsActions =
  | GetCars
  | GetCarsSuccess
  | GetCarsError
  | PostCars
  | PostCarsSuccess
  | PostCarsError
  | UpdateCars
  | UpdateCarsSuccess
  | UpdateCarsError
  | DeleteCars
  | DeleteCarsSuccess
  | DeleteCarsError;