import { Action } from '@ngrx/store';
import { Dealership } from 'src/app/model/dealership.model';


export enum DealershipsActionTypes {
  GET_DEALERSHIPS = 'Get Dealerships',
  GET_DEALERSHIPS_SUCCESS = 'Get Dealerships Success',
  GET_DEALERSHIPS_ERROR = 'Get Dealerships Fail',

  POST_DEALERSHIPS = 'Post Dealerships',
  POST_DEALERSHIPS_SUCCESS = 'Post Dealerships Success',
  POST_DEALERSHIPS_ERROR = 'Post Dealerships Fail',

  UPDATE_DEALERSHIPS = 'Update Dealerships',
  UPDATE_DEALERSHIPS_SUCCESS = 'Update Dealerships Success',
  UPDATE_DEALERSHIPS_ERROR = 'Update Dealerships Fail',

  DELETE_DEALERSHIPS = 'Delete Dealerships',
  DELETE_DEALERSHIPS_SUCCESS = 'Delete Dealerships Success',
  DELETE_DEALERSHIPS_ERROR = 'Delete Dealerships Fail'
}

export class GetDealerships implements Action {
  readonly type = DealershipsActionTypes.GET_DEALERSHIPS;
}
export class GetDealershipsSuccess implements Action {
  readonly type = DealershipsActionTypes.GET_DEALERSHIPS_SUCCESS;
  constructor(public payload: Dealership[]) {}
}
export class GetDealershipsError implements Action {
  readonly type = DealershipsActionTypes.GET_DEALERSHIPS_ERROR;
  constructor(public payload: Error) {}
}

export class PostDealerships implements Action {
  readonly type = DealershipsActionTypes.POST_DEALERSHIPS;
  constructor(public payload: Dealership) {}
}
export class PostDealershipsSuccess implements Action {
  readonly type = DealershipsActionTypes.POST_DEALERSHIPS_SUCCESS;
  constructor(public payload: Dealership) {}
}
export class PostDealershipsError implements Action {
  readonly type = DealershipsActionTypes.POST_DEALERSHIPS_ERROR;
  constructor(public payload: Error) {}
}

export class UpdateDealerships implements Action {
  readonly type = DealershipsActionTypes.UPDATE_DEALERSHIPS;
  constructor(public id:number ,public payload: Dealership) {}
}
export class UpdateDealershipsSuccess implements Action {
  readonly type = DealershipsActionTypes.UPDATE_DEALERSHIPS_SUCCESS;
  constructor(public payload: Dealership) {}
}
export class UpdateDealershipsError implements Action {
  readonly type = DealershipsActionTypes.UPDATE_DEALERSHIPS_ERROR;
  constructor(public payload: Error) {}
}

export class DeleteDealerships implements Action {
  readonly type = DealershipsActionTypes.DELETE_DEALERSHIPS;
  constructor(public id: number) {}
}
export class DeleteDealershipsSuccess implements Action {
  readonly type = DealershipsActionTypes.DELETE_DEALERSHIPS_SUCCESS;
  constructor(public id: number) {}
}
export class DeleteDealershipsError implements Action {
  readonly type = DealershipsActionTypes.DELETE_DEALERSHIPS_ERROR;
  constructor(public payload: Error) {}
}

export type DealershipsActions =
  | GetDealerships
  | GetDealershipsSuccess
  | GetDealershipsError
  | PostDealerships
  | PostDealershipsSuccess
  | PostDealershipsError
  | UpdateDealerships
  | UpdateDealershipsSuccess
  | UpdateDealershipsError
  | DeleteDealerships
  | DeleteDealershipsSuccess
  | DeleteDealershipsError;