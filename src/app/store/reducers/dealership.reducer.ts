import { Dealership } from "src/app/model/dealership.model";
import { DealershipsActions, DealershipsActionTypes } from "../actions/dealership.action";


const initialDealership: DealershipState = {
  loading: false,
  dealerships: []
};

export interface DealershipState {
  loading: boolean;
  dealerships: Dealership[];
}

export function dealershipsReducer(
    state: DealershipState = initialDealership, 
    action: DealershipsActions): DealershipState {
  switch (action.type) {

    case DealershipsActionTypes.GET_DEALERSHIPS:
      return { ...state, loading: true };
    case DealershipsActionTypes.GET_DEALERSHIPS_SUCCESS:
      return { ...state, dealerships: action.payload, loading: false };
    case DealershipsActionTypes.GET_DEALERSHIPS_ERROR:
      return { ...state, loading: false };

    case DealershipsActionTypes.POST_DEALERSHIPS:
      return { ...state, loading: true };
    case DealershipsActionTypes.POST_DEALERSHIPS_SUCCESS:
      return { ...state, dealerships: [...state.dealerships, action.payload], loading: false };
    case DealershipsActionTypes.POST_DEALERSHIPS_ERROR:
      return { ...state, loading: false };

    case DealershipsActionTypes.UPDATE_DEALERSHIPS:
      return { ...state, loading: true };
    case DealershipsActionTypes.UPDATE_DEALERSHIPS_SUCCESS:
      const index = state.dealerships.findIndex(element => element.id === action.payload.id)
      if (index != -1 ) {
        const cars = state.dealerships[index].cars;
        state.dealerships.splice(index, 1, action.payload)
        state.dealerships[index].cars = cars;
      }
      return { ...state, dealerships: [...state.dealerships], loading: false };
    case DealershipsActionTypes.UPDATE_DEALERSHIPS_ERROR:
      return { ...state, loading: false };

    case DealershipsActionTypes.DELETE_DEALERSHIPS:
      return { ...state, loading: true };
    case DealershipsActionTypes.DELETE_DEALERSHIPS_SUCCESS:
      
      return { 
        ...state, 
        dealerships: [...state.dealerships.filter(element => element.id !== action.id)], 
        loading: false 
      };
    case DealershipsActionTypes.DELETE_DEALERSHIPS_ERROR:
      return { ...state, loading: false };

    default:
      return state;
  }
}