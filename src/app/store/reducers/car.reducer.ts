import { Car } from "src/app/model/car.model";
import { CarsActions, CarsActionTypes } from "../actions/car.action";


const initialCar: CarState = {
  loading: false,
  cars: []
};

export interface CarState {
  loading: boolean;
  cars: Car[];
}

export function carsReducer(
    state: CarState = initialCar, 
    action: CarsActions): CarState {
  switch (action.type) {

    case CarsActionTypes.GET_CARS:
      return { ...state, loading: true };
    case CarsActionTypes.GET_CARS_SUCCESS:
      return { ...state, cars: action.payload, loading: false };
    case CarsActionTypes.GET_CARS_ERROR:
      return { ...state, loading: false };

    case CarsActionTypes.POST_CARS:
      return { ...state, loading: true };
    case CarsActionTypes.POST_CARS_SUCCESS:
      return { ...state, cars: [...state.cars, action.payload], loading: false };
    case CarsActionTypes.POST_CARS_ERROR:
      return { ...state, loading: false };

    case CarsActionTypes.UPDATE_CARS:
      return { ...state, loading: true };
    case CarsActionTypes.UPDATE_CARS_SUCCESS:
      const index = state.cars.findIndex(element => element.id === action.payload.id)
      if (index != -1 ) {
        state.cars.splice(index, 1, action.payload)
      }
      return { ...state, cars: [...state.cars], loading: false };
    case CarsActionTypes.UPDATE_CARS_ERROR:
      return { ...state, loading: false };

    case CarsActionTypes.DELETE_CARS:
      return { ...state, loading: true };
    case CarsActionTypes.DELETE_CARS_SUCCESS:
      
      return { 
        ...state, 
        cars: [...state.cars.filter(element => element.id !== action.id)], 
        loading: false 
      };
    case CarsActionTypes.DELETE_CARS_ERROR:
      return { ...state, loading: false };

    default:
      return state;
  }
}