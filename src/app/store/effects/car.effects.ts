import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap, of } from "rxjs";
import { CarService } from "src/app/service/car.service";
import { CarsActionTypes, DeleteCars, DeleteCarsSuccess, GetCars, GetCarsError, GetCarsSuccess, PostCars, PostCarsError, PostCarsSuccess, UpdateCars, UpdateCarsError, UpdateCarsSuccess } from "../actions/car.action";

@Injectable()
export class CarEffects {

  @Effect() getCar$ = this.actions.pipe(
    ofType<GetCars>(CarsActionTypes.GET_CARS),
    mergeMap((data) =>
      this.carService.find(data.id).pipe(
        map((data) => new GetCarsSuccess(data)),
        catchError((error) => of(new GetCarsError(error)))
      )
    )
  );

  @Effect() postCar$ = this.actions.pipe(
    ofType<PostCars>(CarsActionTypes.POST_CARS),
    mergeMap((body) =>
      this.carService.post(body.dealershipId, body.payload).pipe(
        map((data) => new PostCarsSuccess(data)),
        catchError((error) => of(new PostCarsError(error)))
      )
    )
  );

  @Effect() updateCar$ = this.actions.pipe(
    ofType<UpdateCars>(CarsActionTypes.UPDATE_CARS),
    mergeMap((body) =>
      this.carService.put(body.id, body.payload).pipe(
        map((data) => new UpdateCarsSuccess(data)),
        catchError((error) => of(new UpdateCarsError(error)))
      )
    )
  );


  @Effect() deleteCar$ = this.actions.pipe(
    ofType<DeleteCars>(CarsActionTypes.DELETE_CARS),
    mergeMap((body) =>
      this.carService.delete(body.id).pipe(
        map((data) => new DeleteCarsSuccess(body.id)),
        catchError((error) => of(new GetCarsError(error)))
      )
    )
  );

  constructor(
    private actions: Actions,
    private carService: CarService
  ) {}
}