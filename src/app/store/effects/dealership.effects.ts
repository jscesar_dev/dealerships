import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { catchError, map, mergeMap, of } from "rxjs";
import { DealershipService } from "src/app/service/dealership.service";
import { DealershipsActionTypes, DeleteDealerships, DeleteDealershipsSuccess, GetDealerships, GetDealershipsError, GetDealershipsSuccess, PostDealerships, PostDealershipsError, PostDealershipsSuccess, UpdateDealerships, UpdateDealershipsError, UpdateDealershipsSuccess } from "../actions/dealership.action";

@Injectable()
export class DealershipEffects {

  @Effect() getDealership$ = this.actions.pipe(
    ofType<GetDealerships>(DealershipsActionTypes.GET_DEALERSHIPS),
    mergeMap(() =>
      this.dealershipService.find().pipe(
        map((data) => new GetDealershipsSuccess(data)),
        catchError((error) => of(new GetDealershipsError(error)))
      )
    )
  );

  @Effect() postDealership$ = this.actions.pipe(
    ofType<PostDealerships>(DealershipsActionTypes.POST_DEALERSHIPS),
    mergeMap((body) =>
      this.dealershipService.post(body.payload).pipe(
        map((data) => new PostDealershipsSuccess(data)),
        catchError((error) => of(new PostDealershipsError(error)))
      )
    )
  );

  @Effect() updateDealership$ = this.actions.pipe(
    ofType<UpdateDealerships>(DealershipsActionTypes.UPDATE_DEALERSHIPS),
    mergeMap((body) =>
      this.dealershipService.put(body.id, body.payload).pipe(
        map((data) => new UpdateDealershipsSuccess(data)),
        catchError((error) => of(new UpdateDealershipsError(error)))
      )
    )
  );


  @Effect() deleteDealership$ = this.actions.pipe(
    ofType<DeleteDealerships>(DealershipsActionTypes.DELETE_DEALERSHIPS),
    mergeMap((body) =>
      this.dealershipService.delete(body.id).pipe(
        map((data) => new DeleteDealershipsSuccess(body.id)),
        catchError((error) => of(new GetDealershipsError(error)))
      )
    )
  );

  constructor(
    private actions: Actions,
    private dealershipService: DealershipService
  ) {}
}