import { DealershipState } from "./reducers/dealership.reducer";
import {  CarState } from "./reducers/car.reducer";

export interface AppState {
    readonly dealerships: DealershipState,
    readonly cars: CarState,

}