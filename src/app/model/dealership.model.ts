import { Car } from "./car.model";

export interface Dealership {
    id: number;
    name: string;
    totalBudget: number;
    owner: {
        firstName: string;
        lastName: string;
    };
    location: {
        lat: number;
        lon: number;
    };
    remainingBudget?: number;
    amountCars?: number;
    cars?: Car[];
}