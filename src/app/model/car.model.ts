export interface Car {
    id: string;
    brand: string;
    model: string;
    color: string;
    price: number;
    dealershipId?: string;
}