import { Component, Inject, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Car } from 'src/app/model/car.model';
import { PostCars, UpdateCars } from 'src/app/store/actions/car.action';
import { AppState } from 'src/app/store/app.state';

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.scss']
})
export class CarsFormComponent implements OnInit {
  isUpdate: boolean = false;

  carForm = this.fb.group({
    id: [null],
    brand: [null, Validators.required],
    model: [null, Validators.required],
    color: [null, Validators.required],
    dealershipId: [null],
    price: [0, Validators.required],
  });

  constructor(
    public dialogRef: MatDialogRef<CarsFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Car,
    private fb: FormBuilder,
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) { }
  
  ngOnInit(): void {
    if(this.data.id) {
      this.isUpdate = true;
      this.carForm.patchValue(this.data)
    }
    
  }

  createUpdateForm(): void {

    const dealershipId = parseInt(this.route.snapshot.params['id']);

    this.carForm.get('dealershipId').setValue(parseInt(this.data.dealershipId));


    if(this.isUpdate === true) {
      this.store.dispatch(new UpdateCars(this.carForm.value.id, this.carForm.value))
    } else {
      this.store.dispatch(new PostCars(this.data.dealershipId, this.carForm.value))
    }
    this.cancel();
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
