import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject, takeUntil } from 'rxjs';
import { Car } from 'src/app/model/car.model';
import { DeleteCars, GetCars } from 'src/app/store/actions/car.action';
import { AppState } from 'src/app/store/app.state';
import { CarsFormComponent } from './cars-form/cars-form.component';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit, OnDestroy {

  @ViewChild(MatSort) sort: MatSort;

  notifier = new Subject();

  displayedColumns: string[] = ['brand', 'model', 'color', 'price', 'actions'];

  dataSource: MatTableDataSource<any>;

  constructor(private dialog: MatDialog, private store: Store<AppState>, private route: ActivatedRoute) { }

  ngOnInit(): void {


    //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.store.dispatch(new GetCars(this.route.snapshot.params['id']))
    this.store.select(store => store.cars.cars)
    .pipe(
      takeUntil(this.notifier)
    )
    .subscribe(res => {
      
      this.dataSource = new MatTableDataSource(res);
      
      this.dataSource.sort = this.sort;
    })
    
  }

  createCar(): void {
    
    const dealershipId = parseInt(this.route.snapshot.params['id'])
    const car = {}
    
    car['dealershipId'] = dealershipId;
    
    const dialogRef = this.dialog.open(CarsFormComponent, {data: car})
    
  }

  updateCar(car:Car): void {
    const dealershipId = this.route.snapshot.params['id']
    car['dealershipId'] = dealershipId;
    const dialogRef = this.dialog.open(CarsFormComponent, {data: car});
  }

  deleteCar(car: Car): void {
    this.store.dispatch(new DeleteCars(car.id))
  }


  ngOnDestroy() {
    this.notifier.next(null);
    this.notifier.complete()
  }

  applyFilter(event: Event) {
    
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

}
