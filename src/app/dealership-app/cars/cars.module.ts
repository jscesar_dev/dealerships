import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarsComponent } from './cars.component';
import { CarsFormComponent } from './cars-form/cars-form.component';
import { CarsRoutingModule } from './cars-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { MatSortModule } from '@angular/material/sort';



@NgModule({
  declarations: [
    CarsComponent,
    CarsFormComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSortModule,
    SweetAlert2Module.forChild(),
    FormsModule,
    ReactiveFormsModule,
    CarsRoutingModule
  ]
})
export class CarsModule { }
