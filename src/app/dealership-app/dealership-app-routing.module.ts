import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DealershipComponent } from './dealership.component';


const routes: Routes = [
  {
    path: '',
    component: DealershipComponent,
  },
  {
    path: ':id',
    loadChildren: () => import('./cars/cars.module').then(m => m.CarsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealershipAppRoutingModule { }
