import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { filter, map, Subject, takeUntil } from 'rxjs';
import { SweetAlertInput } from 'sweetalert2';
import { Dealership } from '../model/dealership.model';
import { DeleteDealerships, GetDealerships, UpdateDealerships } from '../store/actions/dealership.action';
import { AppState } from '../store/app.state';
import { DealershipFormComponent } from './dealership-form/dealership-form.component';

@Component({
  selector: 'app-dealership',
  templateUrl: './dealership.component.html',
  styleUrls: ['./dealership.component.scss']
})
export class DealershipComponent implements OnInit, OnDestroy {

  @ViewChild(MatSort) sort: MatSort;

  dealerships: Dealership[];

  displayedColumns: string[] = ['name', 'remainingBudget', 'totalBudget', 'amountCars', 'actions'];

  dataSource: MatTableDataSource<any>;

  notifier = new Subject();

  constructor(private dialog: MatDialog, private store: Store<AppState>) { }

  ngOnInit(): void {
    //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.store.dispatch(new GetDealerships())
    this.store.select(store => store.dealerships.dealerships)
    .pipe(
    //  filter(data => data.length > 0),
      takeUntil(this.notifier)
      )
    .subscribe(res => {
      if (res.length > 0) {
        const data = JSON.parse(JSON.stringify(res)) as Dealership[];
        let tot = 0;
        data.forEach(ele => {
          
          if (ele.cars?.length > 0 ) {
            ele.remainingBudget =  ele.totalBudget - ele.cars.reduce((sum, current ) =>  {return sum + current.price} ,0) ;
            ele.amountCars = ele.cars.length;
          } else {
            ele.remainingBudget =  ele.totalBudget
            ele.amountCars = 0
          }

        })
  
        this.dataSource = new MatTableDataSource(data);
      } else {
        this.dataSource = new MatTableDataSource([]);
      }

      this.dataSource.sort = this.sort;

    })
  }

  createDealership(): void {
    const dialogRef = this.dialog.open(DealershipFormComponent)
    
  }

  updateDealership(dealership:Dealership): void {
    const dialogRef = this.dialog.open(DealershipFormComponent, {data: dealership});
    
  }

  deleteDealership(dealership: Dealership): void {
    this.store.dispatch(new DeleteDealerships(dealership.id))
  }

  applyFilter(event: Event) {
    
    const filterValue = (event.target as HTMLInputElement).value;

    this.dataSource.filter = filterValue.trim().toLowerCase();

  }

  ngOnDestroy() {
    this.notifier.next(null);
    this.notifier.complete()
  }

}
