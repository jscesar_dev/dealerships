import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DealershipFormComponent } from './dealership-form.component';

describe('DealershipFormComponent', () => {
  let component: DealershipFormComponent;
  let fixture: ComponentFixture<DealershipFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DealershipFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DealershipFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
