import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Dealership } from 'src/app/model/dealership.model';

import { FormBuilder, Validators } from '@angular/forms';
import { PostDealerships, UpdateDealerships } from 'src/app/store/actions/dealership.action';
import { AppState } from 'src/app/store/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-dealership-form',
  templateUrl: './dealership-form.component.html',
  styleUrls: ['./dealership-form.component.scss']
})



export class DealershipFormComponent implements OnInit {

  isUpdate: boolean = false;

  dealershipForm = this.fb.group({
    id: [null],
    name: [null, Validators.required],
    totalBudget: [0, Validators.required],
    owner: this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
    }),
    location: this.fb.group({
      lat: [null, [Validators.min(-90), Validators.max(90)]],
      lon: [null, [Validators.min(-180), Validators.max(180)]],
    }),
  });

  constructor(
    public dialogRef: MatDialogRef<DealershipFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Dealership,
    private fb: FormBuilder,
    private store: Store<AppState>
  ) { }
  
  ngOnInit(): void {
    
    if(this.data) {
      this.isUpdate = true;
      this.dealershipForm.patchValue(this.data)
    }
    
  }

  createUpdateForm(): void {
    if(this.isUpdate === true) {
      this.store.dispatch(new UpdateDealerships(this.dealershipForm.value.id, this.dealershipForm.value))
    } else {
      this.store.dispatch(new PostDealerships( this.dealershipForm.value))
    }

    this.dialogRef.close();
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
