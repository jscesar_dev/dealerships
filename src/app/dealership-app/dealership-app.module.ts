import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DealershipComponent } from './dealership.component';
import { DealershipAppRoutingModule } from './dealership-app-routing.module';
import {MatTableModule} from '@angular/material/table';
import { DealershipFormComponent } from './dealership-form/dealership-form.component';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
  declarations: [
    DealershipFormComponent,
    DealershipComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSortModule,
    SweetAlert2Module.forChild(),
    FormsModule,
    ReactiveFormsModule,
    DealershipAppRoutingModule
  ]
})
export class DealershipAppModule { }
